import java.util.HashMap;
import java.util.ArrayList;

import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import java.util.Random;
import static spark.Spark.*;


public class App {

  static HashMap<String, String> game = new HashMap<String, String>();

  static ArrayList<Integer> replacementIndices = new ArrayList<Integer>();

  static App myApp = new App();

  //Take user input, convert to string[], replace vowels, and return modified string.
  public String replaceVowels( String phrase ) {
    String[] phraseArray = phrase.split("");
    String vowels = "aAeEiIoOuUyY";
    String output = "";
    for ( int i = 0; i < phraseArray.length; i++ ) {
      if ( vowels.contains(phraseArray[i]) ) {
        phraseArray[i] = "-";
        replacementIndices.add(i);
      }
      output += phraseArray[i];
    }
    return output;
  }

  //Reveal one replaced vowel at a time
  public void revealHint() {
    if (replacementIndices.size() > 0) {
      Random myRandomGenerator = new Random();
      String[] originalArray = game.get("originalPhrase").split("");
      String[] outputArray = game.get("outputPhrase").split("");
      String output = "";
      int random = myRandomGenerator.nextInt(replacementIndices.size());
      outputArray[replacementIndices.get(random)] = originalArray[replacementIndices.get(random)];
      replacementIndices.remove(random);
      for (int i = 0; i < outputArray.length; i ++) {
        output += outputArray[i];
      }
      game.put("outputPhrase", output);
    }
  }

  //Checks user guess aginst originalPHrase
  public Boolean checkGuess ( String original, String guess ) {
    if (guess.toLowerCase().equals(original.toLowerCase())) {
      return true;
    } else {
      return false;
    }
  }

  public static void main(String[] args) {
    staticFileLocation("/public");
    String layout = "templates/layout.vtl";

    //root page
    get("/", (request, response) -> {
        HashMap model = new HashMap();
        model.put("template", "templates/index.vtl" );
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());


    //shows obscured phrase
    post("/results", (request, response) -> {
        HashMap model = new HashMap();
        model.put("template", "templates/results.vtl");
        //Capture originalPhrase
        game.put("originalPhrase", request.queryParams("phrase-input"));
        //Generate outputPhrase
        game.put("outputPhrase", myApp.replaceVowels(game.get("originalPhrase")));
        //Display ouitputPhrase
        model.put("output", game.get("outputPhrase"));
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());

    //shows hint on button click
    get("/hint", (request, response) -> {
        HashMap model = new HashMap();
        model.put("template", "templates/results.vtl");
        myApp.revealHint();
        model.put("output", game.get("outputPhrase"));
        return new ModelAndView(model, "templates/layout.vtl");
      }, new VelocityTemplateEngine());

    //Checks guess against originalPhrase. If correct, shows correct; if incorrect, shows incorrect.
    get("/solve", (request, responce) -> {
      HashMap model = new HashMap();
      model.put("template", "templates/solve.vtl");
      String output = "";
      String guess = request.queryParams("answer");
      if (myApp.checkGuess( game.get("originalPhrase"), guess) ) {
        output = "Your're correct!";
      } else {
        output = "Sorry, wrong answer.";
      }
      model.put("output", output);
      return new ModelAndView(model, "templates/layout.vtl");
    }, new VelocityTemplateEngine());

  }
}
