import org.fluentlenium.adapter.FluentTest;
import org.junit.ClassRule;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static org.assertj.core.api.Assertions.assertThat;

public class AppTest extends FluentTest {
  public WebDriver webDriver = new HtmlUnitDriver();

  @Override
  public WebDriver getDefaultDriver() {
    return webDriver;
  }

  @ClassRule
  public static ServerRule server = new ServerRule();

  @Test
  public void rootTest() {
    goTo("http://localhost:4567/");
    assertThat(pageSource()).contains("Enter a phrase and see if someone else can guess what it is.");
  }

  @Test
  public void removesVowels() {
    goTo("http://localhost:4567/");
    fill("#phrase-input").with("Well hello there. This is a test.");
    submit(".btn");
    assertThat(pageSource()).contains("W-ll h-ll- th-r-. Th-s -s - t-st.");
  }

  @Test
  public void showsHint() {
    goTo("http://localhost:4567/");
    fill("#phrase-input").with("Het");
    submit(".btn");
    submit("#reveal-a-letter");
    assertThat(pageSource()).contains("Het");
  }

  @Test
  public void incorrectAnswer() {
    goTo("http://localhost:4567/");
    fill("#phrase-input").with("Het");
    submit(".btn");
    fill("#answer").with("Hat");
    submit("#solve");
    assertThat(pageSource()).contains("wrong");
  }

  @Test
  public void correctAnswer() {
    goTo("http://localhost:4567/");
    fill("#phrase-input").with("Het");
    submit(".btn");
    fill("#answer").with("Het");
    submit("#solve");
    assertThat(pageSource()).contains("correct");
  }

  @Test
  public void answerCaseInsensitivity() {
    goTo("http://localhost:4567/");
    fill("#phrase-input").with("Het");
    submit(".btn");
    fill("#answer").with("hET");
    submit("#solve");
    assertThat(pageSource()).contains("correct");
  }


}
