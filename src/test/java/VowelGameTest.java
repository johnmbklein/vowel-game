import org.junit.*;
import static org.junit.Assert.*;
import java.io.*;

public class VowelGameTest {

  @Test
  public void replaceVowels_replaceLowerCaseVowelsWithDash_String(){
    App testApp = new App();
    String expected = "-bcd-fgh";
    assertEquals(expected, testApp.replaceVowels("abcdefgh"));
  }

  @Test
  public void replaceVowels_replaceUpperCaseVowelsWithDash_String(){
    App testApp = new App();
    String expected = "-bcd-fgh";
    assertEquals(expected, testApp.replaceVowels("AbcdEfgh"));
  }

  @Test
  public void replaceVowels_maintainConsonantCase_String(){
    App testApp = new App();
    String expected = "-BCD-FGH";
    assertEquals(expected, testApp.replaceVowels("ABCDEFGH"));
  }

  @Test
  public void replaceVowels_allConsonants_String(){
    App testApp = new App();
    String expected = "bcdfghjkl";
    assertEquals(expected, testApp.replaceVowels("bcdfghjkl"));
  }

  @Test
  public void checkGuess_correctGuess_true(){
    App testApp = new App();
    Boolean expected = true;
    assertEquals(expected, testApp.checkGuess("Hello", "Hello"));
  }

  //How can I run junit tests on revealHint? It accepts no inputs and has no outputs.

  @Test
  public void checkGuess_incorrectGuess_false(){
    App testApp = new App();
    Boolean expected = false;
    assertEquals(expected, testApp.checkGuess("Hello", "Bye"));
  }

  @Test
  public void checkGuess_caseInsensitivity_true(){
    App testApp = new App();
    Boolean expected = true;
    assertEquals(expected, testApp.checkGuess("hello", "HELLO"));
  }


}
